import torch.utils.data as data
import torchvision.transforms as transforms
from torchvision.transforms import functional as F

from PIL import Image, ImageOps
import os
import numpy as np
import os.path
import math
import shutil
import torch
import scipy.io

IMG_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.ppm', '.bmp', '.pgm']



class JointRandomFlip(object):
    def __init__(self, rand=True):
        self.rand = rand
    def __call__(self, img, seg):
        randnum = np.random.random()
        #print(randnum)
        if self.rand and randnum < 0.5:
            img = F.hflip(img)
            seg = F.hflip(seg)
        return img, seg
        
        

class JointRandomScale(object):
    def __init__(self, opt_size, rand=True):
        self.rand = rand
        self.size = opt_size
    def __call__(self, img, seg):
        if self.rand:
            width, height = img.size
            f_scale = 0.5 + np.random.randint(0, 10) / 10.0
            #f_scale = max([f_scale, self.size/float(width), self.size/float(height)])
            
            width, height = int(width * f_scale), int(height * f_scale)
            img = F.resize(img, (height, width), Image.BILINEAR)
            seg = F.resize(seg, (height, width), Image.NEAREST)
        return img, seg
    


class JointResize(object):
    def __init__(self, opt_size):
        self.size = opt_size
    def __call__(self, img, seg):
        img = F.resize(img, (self.size, self.size), Image.BILINEAR)
        seg = F.resize(seg, (self.size, self.size), Image.NEAREST)
        return img, seg    
    
    
""" 
   
class JointRandomCrop2(object):
    def __init__(self, opt_size, rand=True, ispad=True, ignore_label=255):
        self.ignore_label = ignore_label
        self.rand = rand
        self.ispad = ispad
        self.size = (int(opt_size), int(opt_size))
    def __call__(self, img, seg):
        width, height = img.size
        pad_h = max(self.size[0] - height, 0)
        pad_w = max(self.size[1] - width, 0)
        padding = ((pad_w+1)/2, (pad_h+1)/2)

        if (pad_h > 0 or pad_w > 0) and self.ispad:
            img = F.pad(img, padding, fill=0)
            seg = F.pad(seg, padding, fill=self.ignore_label)
        if self.rand:
            params = transforms.RandomCrop.get_params(img, self.size)
            #img = F.pad(img, 0)
            img = F.crop(img, *params)
            #seg = F.pad(seg, 1)
            seg = F.crop(seg, *params)
        else:
            img = F.center_crop(img, self.size)
            seg = F.center_crop(seg, self.size)
        return img, seg
"""        

        
        
class JointRandomCrop(object):
    def __init__(self, opt_size, rand=True):
        self.rand = rand
        self.size = (int(opt_size), int(opt_size))
    def __call__(self, img, seg):
        w, h = img.size
        th, tw = self.size
        if self.rand:
            i = 0 if h==th else np.random.randint(min(h-th, 0), max(h-th, 0))
            j = 0 if w==tw else np.random.randint(min(w-tw, 0), max(w-tw, 0))
            params = (i, j, th, tw)
            img = F.crop(img, *params)
            seg = F.crop(seg, *params)
        return img, seg

class JointPadForTest(object):
    def __init__(self, opt_size, pad_value=255):
        self.pad_value = pad_value
        self.size = (int(opt_size), int(opt_size))
    def __call__(self, img):
        lr, tb = tuple(x-y for x, y in zip(self.size, img.size))
        left, right, top, bottom = lr//2, lr - (lr//2), tb//2, tb - (tb//2)
        img = F.pad(img, (left,top,right,bottom))
        return img, (left, right, top, bottom)

class JointPad(object):
    def __init__(self, opt_size, pad_value=255, switch=True):
        self.pad_value = pad_value
        self.size = (int(opt_size), int(opt_size))
        self.switch = switch
    def __call__(self, img, seg):
        if self.switch:
            lr, tb = tuple(x-y for x, y in zip(self.size, img.size))
            left, right, top, bottom = lr//2, lr - (lr//2), tb//2, tb - (tb//2)
            img = F.pad(img, (left,top,right,bottom))
            seg = F.pad(seg, (left,top,right,bottom), fill=self.pad_value)
        return img, seg

class JointPostProcess(object):
    def __init__(self, norm=True):
        self.norm = norm
    def __call__(self, img, seg):
        img = F.to_tensor(img)
        img = (img*255.0).float()
        seg = F.to_tensor(seg)
        seg = (seg*255.0).long()
        if self.norm:
            # img mode 'BGR'
            img = F.normalize(img, [104.00698793,116.66876762,122.67891434], [1.0,1.0,1.0])
        return img, seg

class JointPostProcessForTest(object):
    def __init__(self, norm=True):
        self.norm = norm
    def __call__(self, img):
        img = F.to_tensor(img)
        img = (img*255.0).float()
        if self.norm:
            # img mode 'BGR'
            img = F.normalize(img, [104.00698793,116.66876762,122.67891434], [1.0,1.0,1.0])
        return img
   
        
class JointCompose(object):
    def __init__(self, transforms):
        self.transforms = transforms
    def __call__(self, img):
        for t in self.transforms:
            img = t(*img)
        return img 




    
def is_image_file(filename):
    """Checks if a file is an image.
    Args:
        filename (string): path to a file
    Returns:
        bool: True if the filename ends with a known image extension
    """
    filename_lower = filename.lower()
    return any(filename_lower.endswith(ext) for ext in IMG_EXTENSIONS)



#because model will be initialized by pretrained caffe model, image should be loaded in BGR mode.

def pil_loader(path, mode='RGB'):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            if mode == 'BGR':
                img = img.convert('RGB')
                r, g, b = img.split()
                img = Image.merge('RGB', (b, g, r))
            else:
                img = img.convert(mode)
            return img

        
class ImageFolderForPascalContext(data.Dataset):
    def __init__(self, root, data_list, opt_size, flip=True, scale=True, crop=True, pad=True, resize=False, test=False):
        self.root = root
        self.test = test
        self.opt_size = opt_size
        self.img_name = []
        self.lab_name = []
        self.flip = flip
        self.scale = scale
        self.crop = crop
        self.pad = pad
        self.resize = resize
        self.context_path = os.path.join(self.root, 'pascalcontext')
        self.indices = open('{}/ImageSets/Main/{}'.format(self.root, data_list),'r').read().splitlines()
        self.labels_400 = [label.decode('UTF-8').replace(' ', '') for idx, label in np.genfromtxt(os.path.join(self.context_path, '400_labels.txt'), delimiter=':', dtype=None)]
        self.labels_59 = [label.decode('UTF-8').replace(' ', '') for idx, label in np.genfromtxt(os.path.join(self.context_path, '59_labels.txt'), delimiter=':', dtype=None)]
        self.ToPILImage = transforms.ToPILImage()
        
    def __getitem__(self, index):
        label_400 = scipy.io.loadmat('{}/trainval/{}.mat'.format(self.context_path, self.indices[index]))['LabelMap']
        label = np.zeros_like(label_400, dtype=np.uint8)
        for idx, l in enumerate(self.labels_59):
            idx_400 = self.labels_400.index(l) + 1
            label[label_400 == idx_400] = idx + 1
        label = label[:,:,np.newaxis]
        label = label - 1
        lab_ori = self.ToPILImage(label)
        img_ori = pil_loader('{}/JPEGImages/{}.jpg'.format(self.root, self.indices[index]), 'BGR')
        
        if self.resize:
            transform = JointCompose([JointRandomFlip(self.flip),
                                          JointResize(self.opt_size),
                                          JointPostProcess(),])
        else:
            transform = JointCompose([JointRandomFlip(self.flip),
                                          JointRandomScale(self.opt_size, self.scale),
                                          JointRandomCrop(self.opt_size, self.crop),
                                          JointPad(self.opt_size, switch=self.pad),
                                          JointPostProcess(),])
        img, lab = transform([img_ori, lab_ori])
        return img, lab
    def __len__(self):
        return len(self.indices)

class ImageFolderForVOC2012(data.Dataset):
    def __init__(self, root, data_list, opt_size, flip=True, scale=True, crop=True, pad=True, resize=False, test=False):
        self.root = root
        self.test = test
        self.opt_size = opt_size     
        self.img_names = []
        self.lab_names = []
        self.flip = flip
        self.scale = scale
        self.crop = crop
        self.pad = pad
        self.resize = resize
        
        file_to_read = open(os.path.join(self.root, 'ImageSets/SegmentationAug' ,data_list), 'r')
        while True:
            lines = file_to_read.readline()
            lines = lines.strip('\n')
            if not lines:
                break
            terms = lines.split(' ')
            self.img_names.append(terms[0])
            if not self.test:
                self.lab_names.append(terms[1])
        file_to_read.close()
        

        
    def __getitem__(self, index):
        if not self.test:
            img_name = self.img_names[index]
            lab_name = self.lab_names[index]
            img_ori = pil_loader(self.root + img_name, 'BGR')
            lab_ori = pil_loader(self.root + lab_name, 'P')
            if self.resize:
                transform = JointCompose([JointRandomFlip(self.flip), 
                                              JointResize(self.opt_size), 
                                              JointPostProcess(),])            
            else:
                transform = JointCompose([JointRandomFlip(self.flip), 
                                              JointRandomScale(self.opt_size, self.scale), 
                                              JointRandomCrop(self.opt_size, self.crop),
                                              JointPad(self.opt_size, switch=self.pad), 
                                              JointPostProcess(),])
 
            img, lab = transform([img_ori, lab_ori])
            return img, lab
        else:
            img_name = self.img_names[index]
            img_ori = pil_loader(self.root + img_name, 'BGR')
            transformPad = JointPadForTest(self.opt_size)
            transformPostProcess = JointPostProcessForTest()
            img, pad_size = transformPad(img_ori)
            img = transformPostProcess(img)
            name = self.img_names[index][:-4]
            return img, name, pad_size
    
    
    def __len__(self):
        return len(self.img_names)
