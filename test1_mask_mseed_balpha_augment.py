import os
import sys
import time
import shutil
import random
import numpy as np
import torchnet as tnt

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.utils as vutils
import torchvision.models as models
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from torch.utils import data
from tensorboardX import SummaryWriter

import model_resnet_dif_mask as model
import basic_function as func
import CustomDataset_mseed as CustomDataset

from IPython.core import debugger
debug = debugger.Pdb().set_trace


opt_manualSeed = 1000#random.randint(1, 10000)  # fix seed
print("Random Seed: ", opt_manualSeed)
np.random.seed(opt_manualSeed)
torch.manual_seed(opt_manualSeed)
torch.cuda.manual_seed_all(opt_manualSeed)

#cudnn.enabled = True
cudnn.benchmark = True
#cudnn.deterministic = True
os.environ["CUDA_VISIBLE_DEVICES"] = '2, 3'



arg_epochs = 100
arg_numclasses = 21
arg_lr = 2.5e-4
arg_momentum = 0.9
arg_wdecay = 0.0005



def train(train_loader, model, criterion, optimizer, basic_lr, epoch, epoch_total, writer, num_label=21, iter_size =10., print_freq=20):
    model.train()
    losses = func.AverageMeter()    
    iter_loss = 0
    end = time.time()
      
    for i, batch in enumerate(train_loader):
        img, gt, name, shape = batch
        
        batch_size = img.size()[0]
        input_size = img.size()[2:4]
        inter_size = [41, 41]

        img_v = Variable(img).cuda()
        gt_v = Variable(gt).cuda()

        seed, pred, pred_P = model(img_v)
        alpha = model.module.get_alpha()
        
        pred_seed_up = F.upsample(seed, size=input_size, mode='bilinear')      
        pred_sg_up = F.upsample(pred[-1], size=input_size, mode='bilinear')         
        
        loss_sg = criterion(pred_sg_up, gt_v.squeeze(1))
        loss = loss_sg 
        losses.update(loss.data[0], img.size(0))
        
        current_lr = func.adjust_learning_rate(optimizer, basic_lr, epoch*len(train_loader)+i, epoch_total*len(train_loader))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time = time.time() - end
        end = time.time()   
        writer.add_scalar('Loss_train', loss, epoch*len(train_loader)+i)
        writer.add_scalar('lr', current_lr, epoch*len(train_loader)+i)        
 
        
        if i % print_freq == 0:
            print(alpha)
            print('Train: [{0}][{1}/{2}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time, loss=losses))

            show_img = vutils.make_grid(func.BGR2RGB(img), normalize=True, scale_each=True, pad_value=0)   
            writer.add_image('img', show_img, epoch*len(train_loader)+i)
            
            pred_sg_map = func.ColorMapping(func.map_decode(torch.max(pred_sg_up.data,1,keepdim=True)[1]))
            show_pred_sg = vutils.make_grid(pred_sg_map, normalize=False, pad_value=1) 
            writer.add_image('sg', show_pred_sg, epoch*len(train_loader)+i)
                       
            #sg_confi = torch.max(F.softmax(pred_sg_up, 1),1,keepdim=True)[0]
            #show_sg_confi = vutils.make_grid(sg_confi.data, normalize=False, pad_value=1) 
            #writer.add_image('sg_confi', show_sg_confi, epoch*len(train_loader)+i)
            
            pred_seed_map = func.ColorMapping(func.map_decode(torch.max(pred_seed_up.data,1,keepdim=True)[1]))
            show_pred_seed = vutils.make_grid(pred_seed_map, normalize=False, pad_value=1) 
            writer.add_image('seed', show_pred_seed, epoch*len(train_loader)+i)            
            
            #seed_confi = torch.max(F.softmax(pred_seed_up, 1),1,keepdim=True)[0]
            #show_seed_confi = vutils.make_grid(seed_confi.data, normalize=False, pad_value=1) 
            #writer.add_image('seed_confi', show_seed_confi, epoch*len(train_loader)+i)            
            
            gt_map = func.ColorMapping(func.map_decode(gt))
            show_gt = vutils.make_grid(gt_map, normalize=False, pad_value=1)   
            writer.add_image('gt', show_gt, epoch*len(train_loader)+i)
    
            #show_cluster = vutils.make_grid(cluster.unsqueeze(1), normalize=False, pad_value=1)   
            #writer.add_image('cluster', show_cluster, epoch*len(train_loader)+i)

            #show_pred_cluster = vutils.make_grid(F.sigmoid(pred_cluster_v).unsqueeze(1).data, normalize=False, pad_value=1) 
            #writer.add_image('pred_cluster', show_pred_cluster, epoch*len(train_loader)+i)                                   

                
def validate_tnt(val_loader, model, criterion, epoch, writer, num_label=21, print_freq=20):
    model.eval()
    
    confusion_meter = tnt.meter.ConfusionMeter(num_label+1, normalized=False)
    losses = func.AverageMeter()

    end = time.time()
    for i, batch in enumerate(val_loader):
        img, gt, name, shape = batch
        th, tw = shape[0,0], shape[0,1] 
        h, w = img.size()[2:4]
        up = int(round((h - th) / 2.))
        le = int(round((w - tw) / 2.))
                
        #img = img[:,:,up:up+th,le:le+tw].contiguous()
        gt = gt[:,:,up:up+th,le:le+tw].contiguous()
    
        batch_size = img.size()[0]
        input_size = img.size()[2:4]

        img_v = Variable(img, volatile=True).cuda()
        gt_v = Variable(gt, volatile=True).cuda()
        
        seed, pred, pred_P = model(img_v)
        alpha = model.module.get_alpha()
        
        pred_seed_up = F.upsample(seed, size=input_size, mode='bilinear') 
        pred_seed_up = pred_seed_up[:,:,up:up+th,le:le+tw].contiguous() 
        pred_sg_up = F.upsample(pred[-1], size=input_size, mode='bilinear')         
        pred_sg_up = pred_sg_up[:,:,up:up+th,le:le+tw].contiguous() 
        
        loss = criterion(pred_sg_up, gt_v.squeeze(1))
        
        lab = gt[0][0].view(-1).clone()
        lab[lab==255]=21
        
        seg = F.softmax(pred_sg_up, 1).data[0].view(num_label,-1).t()
        ambiguious = torch.zeros(seg.size(0),1).cuda()
        seg = torch.cat((seg, ambiguious), 1)

        confusion_meter.add(seg, lab)
 
        losses.update(loss.data[0], img.size(0))
        batch_time = time.time() - end
        end = time.time()   
        
        if i % print_freq == 0:
            print(alpha)
            print('Valid: [{0}/{1}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses))       
    
    confusion_matrix = confusion_meter.value()
    confusion_matrix = confusion_matrix[0:-1,0:-1]
   
    inter = np.diag(confusion_matrix)
    union = confusion_matrix.sum(1).clip(min=1e-12) + confusion_matrix.sum(0).clip(min=1e-12) - inter  
    
    mean_iou_ind = inter/union
    mean_iou_all = mean_iou_ind.mean()    
    
    print(' * IOU_All {iou}'.format(iou=mean_iou_all))
    print(' * IOU_Ind {iou}'.format(iou=mean_iou_ind))

    if writer!=None:
        writer.add_scalar('Loss_valid', losses.avg, epoch)
        writer.add_scalar('IOU_valid', mean_iou_all, epoch)

    return mean_iou_all, mean_iou_ind     


#def init_fn(worker_id):
#    torch.manual_seed(opt_manualSeed + worker_id)

train_dataset=CustomDataset.ImageFolder('/home/jump/data/Pascal_VOC/VOCdevkit/VOC2012', 'train.txt', 321, flip=True, scale=True, crop=True, resize=False)
#train_data_loader = data.DataLoader(train_dataset, num_workers=2, batch_size=16, shuffle=True, worker_init_fn=init_fn)
train_data_loader = data.DataLoader(train_dataset, num_workers=0, batch_size=16, shuffle=True)

val_dataset=CustomDataset.ImageFolder('/home/jump/data/Pascal_VOC/VOCdevkit/VOC2012', 'val.txt', 505, flip=False, scale=False, crop=False, resize=False)
val_data_loader = data.DataLoader(val_dataset, num_workers=2, batch_size=1, shuffle=False) 


#513


resnet = models.__dict__['resnet18'](pretrained=True)
DifNet = model.Res_Deeplab(num_classes=arg_numclasses)
DifNet.model_sed = func.param_restore(DifNet.model_sed, resnet.state_dict())
DifNet.model_dif = func.param_restore(DifNet.model_dif, resnet.state_dict())
DifNet = torch.nn.DataParallel(DifNet, device_ids=[0, 1])
DifNet = DifNet.cuda()


optimizer = optim.SGD(DifNet.parameters(),lr=arg_lr, momentum=arg_momentum, weight_decay=arg_wdecay)
criterion = func.SegLoss(255)


writer = SummaryWriter('./runs/test1_mask_shuffle_mseed_balpha_augment_100epo')
save_dir = writer.log_dir
best_iou = 0
best_iou_ind = 0
best_alpha = 0


for epoch in range(arg_epochs):

    # train for one epoch
    train(train_data_loader, DifNet, criterion, optimizer, arg_lr, epoch, arg_epochs, writer)

    # evaluate on validation set
    iou_all, iou_ind = validate_tnt(val_data_loader, DifNet, criterion, epoch, writer)

    # remember best prec@1 and save checkpoint
    is_best = iou_all > best_iou
    best_iou = iou_all if is_best else best_iou
    best_iou_ind = iou_ind if is_best else best_iou_ind
    best_alpha = DifNet.module.get_alpha() if is_best else best_alpha
    func.save_checkpoint({
        'epoch': epoch + 1,
        'state_dict': DifNet.state_dict(),
        'best_iou': (best_iou, best_iou_ind),
        'best_alpha': best_alpha,
        'optimizer': optimizer.state_dict(),
    }, is_best, tag='valid', check_dir=save_dir)
   
