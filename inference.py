import os
import sys
import time
import shutil
import random
import numpy as np
import torchnet as tnt
from params import MyArgumentParser
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.utils as vutils
import torchvision.models as models
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from torch.utils import data
from tensorboardX import SummaryWriter
from datetime import datetime
import socket
import pickle
from PIL import Image

import threading

import model_resnet_dif_mask as model_dif
import model_resnet_nodif as model_nodif
import basic_function as func
import CustomDataset_mseed as CustomDataset

cmap = torch.load('../colormap')
np.set_printoptions(threshold=np.inf)
parserWarpper = MyArgumentParser(inference=True)
parser = parserWarpper.get_parser()
args = parser.parse_args()

opt_manualSeed = 1000#random.randint(1, 10000)  # fix seed
print("Random Seed: ", opt_manualSeed)
np.random.seed(opt_manualSeed)
torch.manual_seed(opt_manualSeed)
torch.cuda.manual_seed_all(opt_manualSeed)

#cudnn.enabled = True
cudnn.benchmark = True
#cudnn.deterministic = True
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpus



arg_epochs = args.epochs
arg_numclasses = 21
arg_lr = args.lr
arg_momentum = args.momentum
arg_wdecay = args.weight_decay
arg_dataset = args.inference_dataset

def preprocess_data(data):
    for data_item in data:
        data_item = data_item.cpu().half()
        return data
def save_internal_data(path, idx, data):
    torch.save(data, path + idx + '.data')

def ToRGB(image, cmap):
    print(image[1:20,1:20])
    out = Image.fromarray(image, 'P')
    out.putpalette(cmap)
    return out 

def inference(val_loader, model, num_label=21, print_freq=1):
    model.eval()
    lock = threading.Lock()
    threadlist = []
    losses = func.AverageMeter()
    epoch_end = time.time()
    end = time.time()
    for i, batch in enumerate(val_loader):
        img, name, shape = batch

        batch_size = img.size()[0]
        input_size = img.size()[2:4]

        img_v = Variable(img, volatile=True).cuda()
        
        if args.model_type == 'dif':
            if args.internal_data:
                pred, P = model(img_v)
                P = preprocess_data(P)
            else:
                pred = model(img_v)
            alpha = model.get_alpha()
        else:
            pred = model(img_v)



        pred_sg_up = F.upsample(pred, size=input_size, mode='bilinear')
        pred_sg_up_label = pred_sg_up.max(1)[1]
        pred_sg_up_label_crop = pred_sg_up_label.cpu().data.squeeze()[shape[2][0]:-shape[3][0],shape[0][0]:-shape[1][0]]
        pred_sg_up_label_crop_color = ToRGB(pred_sg_up_label_crop.numpy().astype(np.uint8), cmap)
        print(name)
        pred_sg_up_label_crop_color.save(args.result_path + '/submission/'+ arg_dataset + '/' + name[0][12:] + '.png')
        
        batch_time = time.time() - end
        end = time.time()

        if i % print_freq == 0:
            print('Valid: [{0}/{1}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses))
            if args.model_type == 'dif':
                print(alpha)
                if args.internal_data:
                    def stream_data_to_external_files(idx, P):
                        lock.acquire()
                        save_internal_data(args.args.result_path + '/internal_data/', idx, P)
                        lock.release()
                    threadlist.append(threading.Thread(target=stream_data_to_external_files, args=(i, P)))
                    threadlist[-1].start()
    print('main thread finished, waiting for IO threads...')
    for thread in threadlist:
        thread.join()

    epoch_time = time.time() - epoch_end
    print('Train time : {:.2f}min'.format(epoch_time/60))
if args.dataset == 'VOC2012':
    val_dataset=CustomDataset.ImageFolderForVOC2012(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True, test=True)
elif args.dataset == 'PascalContext':
    val_dataset=CustomDataset.ImageFolderForPascalContext(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True, test=True)

val_data_loader = data.DataLoader(val_dataset, num_workers=1, batch_size=1, shuffle=False)

if '+' in args.layers:
    layers_sed = args.layers.split('+')[0]
    layers_dif = args.layers.split('+')[1]
else:
    layers_sed = args.layers
    layers_dif = args.layers
if args.model_type == 'dif':
    DifNet = model_dif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
elif args.model_type == 'nodif':
    DifNet = model_nodif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
    DifNet = func.param_restore(DifNet, resnet_sed.state_dict())
else:
        print('Unknow value of model_type: {}'.format(args.model_type))
        exit()

#load checkpoint file
checkpoint_file = torch.load(args.checkpoint_path)
checkpoint_file_new = {entry[7:] : checkpoint_file['state_dict'][entry].cpu() for entry in checkpoint_file['state_dict'].keys()}
DifNet.load_state_dict(checkpoint_file_new)
"""
#replace the forward function to save the internal data to the external storage
if args.internal_data :
    def forward(self, x):
        sed = self.model_sed(x)
        mask = self.mask_layer(sed)
        sed = sed * mask
        
        dif = self.model_dif(x)
        
        pred0, P0 = self.diffuse0(x, sed, sed)
        pred1, P1 = self.diffuse1(dif[0], pred0, sed)
        pred2, P2 = self.diffuse2(dif[1], pred1, sed)
        pred3, P3 = self.diffuse3(dif[2], pred2, sed)
        pred4, P4 = self.diffuse4(dif[3], pred3, sed)
        
        return mask, sed, pred4, {x, P0, P1, P2, P3, P4}
else:
    def forward(self, x):
        sed = self.model_sed(x)
        mask = self.mask_layer(sed)
        sed = sed * mask

        dif = self.model_dif(x)

        pred0, P0 = self.diffuse0(x, sed, sed)
        pred1, P1 = self.diffuse1(dif[0], pred0, sed)
        pred2, P2 = self.diffuse2(dif[1], pred1, sed)
        pred3, P3 = self.diffuse3(dif[2], pred2, sed)
        pred4, P4 = self.diffuse4(dif[3], pred3, sed)

        return mask, sed, pred4
"""
#DifNet.forward = forward
DifNet = DifNet.cuda()
inference(val_data_loader, DifNet)
