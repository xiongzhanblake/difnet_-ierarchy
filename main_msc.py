import os
import sys
import time
import shutil
import random
import numpy as np
import torchnet as tnt
from params import MyArgumentParser
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.utils as vutils
import torchvision.models as models
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from torch.utils import data
from tensorboardX import SummaryWriter
from datetime import datetime
import socket

import threading

import model_resnet_dif_mask_mu_res as model_dif
import model_resnet_nodif as model_nodif
import basic_function as func
import CustomDataset_mseed as CustomDataset

from IPython.core import debugger
debug = debugger.Pdb().set_trace

parserWarpper = MyArgumentParser()
parser = parserWarpper.get_parser()
args = parser.parse_args()

opt_manualSeed = 1000#random.randint(1, 10000)  # fix seed
print("Random Seed: ", opt_manualSeed)
np.random.seed(opt_manualSeed)
torch.manual_seed(opt_manualSeed)
torch.cuda.manual_seed_all(opt_manualSeed)

#cudnn.enabled = True
cudnn.benchmark = True
#cudnn.deterministic = True
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpus



arg_epochs = args.epochs
arg_numclasses = 21
arg_lr = args.lr
arg_momentum = args.momentum
arg_wdecay = args.weight_decay



def train(train_loader, model, criterion, optimizer, basic_lr, epoch, epoch_total, writer, num_label=21, print_freq=100):
    lock = threading.Lock()
    model.train()
    losses = func.AverageMeter()
    iter_loss = 0
    epoch_end = time.time()
    end = time.time()
    threadlist = []
    for i, batch in enumerate(train_loader):
        img, gt, name = batch

        batch_size = img.size()[0]
        input_size = img.size()[2:4]
        inter_size = [41, 41]
        
        interpo1 = nn.Upsample(size=input_size, mode='bilinear')
        interpo2 = nn.Upsample(size=inter_size, mode='bilinear')     
        
        img075 = nn.Upsample(size=(int(input_size[0]*0.75), int(input_size[1]*0.75)), mode='bilinear')(img)
        img050 = nn.Upsample(size=(int(input_size[0]*0.50), int(input_size[1]*0.50)), mode='bilinear')(img)       
        
        img100_v = Variable(img).cuda()
        img075_v = Variable(img075.data).cuda()
        img050_v = Variable(img050.data).cuda()
        gt_v = Variable(gt).cuda()
        
        if args.model_type == 'dif':
            mask100, seed100, pred100 = model(img100_v)
            mask075, seed075, pred075 = model(img075_v)
            mask050, seed050, pred050 = model(img050_v)
            pred_seed_up = interpo1(seed100)
            #pred_mask_up = F.upsample(mask100, size=input_size, mode='bilinear')
            #alpha = model.module.get_alpha()
            #beta = model.module.get_beta()
        else:
            pred100 = model(img100_v)
            pred075 = model(img075_v)
            pred050 = model(img050_v)
          
        pred_sg_up = interpo1(torch.max(torch.stack([interpo2(pred100), interpo2(pred075), interpo2(pred050)]), dim=0)[0])
                
        loss_sg = criterion(pred_sg_up, gt_v.squeeze(1))
        loss_sg100 = criterion(pred100, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred100.size()[2:4]).long())
        loss_sg075 = criterion(pred075, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred075.size()[2:4]).long())
        loss_sg050 = criterion(pred050, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred050.size()[2:4]).long())
        
        loss = loss_sg + loss_sg100 + loss_sg075 + loss_sg050
        losses.update(loss.data[0], img.size(0))

        current_lr = func.adjust_learning_rate(optimizer, basic_lr, epoch*len(train_loader)+i, epoch_total*len(train_loader))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time = time.time() - end
        end = time.time()
        writer.add_scalar('Loss_train', loss, epoch*len(train_loader)+i)
        writer.add_scalar('lr', current_lr, epoch*len(train_loader)+i)


        if i % print_freq == 0:
            print('Train: [{0}][{1}/{2}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time, loss=losses))
            if args.model_type == 'dif':
                def stream_data_to_tensorboard(img, pred_sg_up, pred_seed_up, gt):
                    lock.acquire()
                    show_img = vutils.make_grid(func.BGR2RGB(img), normalize=True, scale_each=True, pad_value=0)
                    writer.add_image('img', show_img, epoch*len(train_loader)+i)
                    
                    pred_sg_map = func.ColorMapping(func.map_decode(torch.max(pred_sg_up,1,keepdim=True)[1]))
                    show_pred_sg = vutils.make_grid(pred_sg_map, normalize=False, pad_value=1)
                    writer.add_image('sg', show_pred_sg, epoch*len(train_loader)+i)

                    pred_seed_map = func.ColorMapping(func.map_decode(torch.max(pred_seed_up,1,keepdim=True)[1]))
                    show_pred_seed = vutils.make_grid(pred_seed_map, normalize=False, pad_value=1)
                    writer.add_image('seed', show_pred_seed, epoch*len(train_loader)+i)

                    gt_map = func.ColorMapping(func.map_decode(gt))
                    show_gt = vutils.make_grid(gt_map, normalize=False, pad_value=1)
                    writer.add_image('gt', show_gt, epoch*len(train_loader)+i)
                    lock.release()
                    
                threadlist.append(threading.Thread(target=stream_data_to_tensorboard, args=(img[1:5,::], pred_sg_up.data[1:5,::].cpu(), pred_seed_up.data[1:5,::].cpu(), gt[1:5,::])))
                threadlist[-1].start()
                
    print('main thread finished, waiting for IO threads...')
    for thread in threadlist:
        thread.join()

    epoch_time = time.time() - epoch_end
    print('Train time : {:.2f}min'.format(epoch_time/60))

    
    
def validate_tnt(val_loader, model, criterion, epoch, writer, num_label=21, print_freq=100):
    model.eval()
    lock = threading.Lock()
    threadlist = []
    confusion_meter = tnt.meter.ConfusionMeter(num_label, normalized=False)
    losses = func.AverageMeter()
    epoch_end = time.time()
    end = time.time()
    for i, batch in enumerate(val_loader):
        img, gt, name = batch

        batch_size = img.size()[0]
        input_size = img.size()[2:4]
        inter_size = [64, 64]
        
        interpo1 = nn.Upsample(size=input_size, mode='bilinear')
        interpo2 = nn.Upsample(size=inter_size, mode='bilinear')
        
        img075 = nn.Upsample(size=(int(input_size[0]*0.75), int(input_size[1]*0.75)), mode='bilinear')(img)
        img050 = nn.Upsample(size=(int(input_size[0]*0.50), int(input_size[1]*0.50)), mode='bilinear')(img)       
        
        img100_v = Variable(img, volatile=True).cuda()
        img075_v = Variable(img075.data, volatile=True).cuda()
        img050_v = Variable(img050.data, volatile=True).cuda()
        gt_v = Variable(gt, volatile=True).cuda()

        if args.model_type == 'dif':
            mask100, seed100, pred100 = model(img100_v)
            mask075, seed075, pred075 = model(img075_v)
            mask050, seed050, pred050 = model(img050_v)
            pred_seed_up = interpo1(seed100)
            #pred_mask_up = F.upsample(mask100, size=input_size, mode='bilinear')
            #alpha = model.module.get_alpha()
            #beta = model.module.get_beta()
        else:
            pred100 = model(img100_v)
            pred075 = model(img075_v)
            pred050 = model(img050_v)
          
        pred_sg_up = interpo1(torch.max(torch.stack([interpo2(pred100), interpo2(pred075), interpo2(pred050)]), dim=0)[0])
                
        loss_sg = criterion(pred_sg_up, gt_v.squeeze(1))
        loss_sg100 = criterion(pred100, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred100.size()[2:4]).long())
        loss_sg075 = criterion(pred075, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred075.size()[2:4]).long())
        loss_sg050 = criterion(pred050, F.adaptive_max_pool2d(gt_v.squeeze(1).float(), pred050.size()[2:4]).long())
        
        loss = loss_sg + loss_sg100 + loss_sg075 + loss_sg050
        
        valid_pixel = gt.ne(255)
        pred_sg_up_label = pred_sg_up.max(1)[1]
        
        confusion_meter.add(pred_sg_up_label.data.cpu()[valid_pixel], gt.cpu()[valid_pixel])
        losses.update(loss.data[0], img.size(0))
        batch_time = time.time() - end
        end = time.time()

        if i % print_freq == 0:
            print('Valid: [{0}/{1}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses))
            if args.model_type == 'dif':
                def stream_data_to_tensorboard(img, pred_sg_up, gt):
                    lock.acquire()
                    show_img = vutils.make_grid(func.BGR2RGB(img), normalize=True, scale_each=True, pad_value=0)
                    writer.add_image('img_valid', show_img, epoch*len(val_loader)+i)

                    pred_sg_map = func.ColorMapping(func.map_decode(torch.max(pred_sg_up,1,keepdim=True)[1]))
                    show_pred_sg = vutils.make_grid(pred_sg_map, normalize=False, pad_value=1)
                    writer.add_image('sg_valid', show_pred_sg, epoch*len(val_loader)+i)
                
                    gt_map = func.ColorMapping(func.map_decode(gt))
                    show_gt = vutils.make_grid(gt_map, normalize=False, pad_value=1)
                    writer.add_image('gt_valid', show_gt, epoch*len(val_loader)+i)
                    lock.release()
                    
                threadlist.append(threading.Thread(target=stream_data_to_tensorboard, args=(img[1:5,::], pred_sg_up.data[1:5,::].cpu(), gt[1:5,::])))
                threadlist[-1].start()
                
    print('main thread finished, waiting for IO threads...')
    for thread in threadlist:
        thread.join()
    confusion_matrix = confusion_meter.value()

    inter = np.diag(confusion_matrix)
    union = confusion_matrix.sum(1).clip(min=1e-12) + confusion_matrix.sum(0).clip(min=1e-12) - inter
    
    if args.model_type == 'dif':    
        alpha = model.module.get_alpha()
        beta = model.module.get_beta()    
        print('alpha: {0}\t'.format(alpha))
        print('beta: {0}\t'.format(beta))    
    
    mean_iou_ind = inter/union
    mean_iou_all = mean_iou_ind.mean()
    print(' * IOU_All {iou}'.format(iou=mean_iou_all))
    print(' * IOU_Ind {iou}'.format(iou=mean_iou_ind))

    if writer!=None:
        writer.add_scalar('Loss_valid', losses.avg, epoch)
        writer.add_scalar('IOU_valid', mean_iou_all, epoch)
    epoch_time = time.time() - epoch_end
    print('Train time : {:.2f}min'.format(epoch_time/60))
    return mean_iou_all, mean_iou_ind


#def init_fn(worker_id):
#    torch.manual_seed(opt_manualSeed + worker_id)



train_dataset=CustomDataset.ImageFolder(args.dataset_path, 'train.txt', 321, flip=True, scale=True, crop=True, resize=False, pad=False)
#train_data_loader = data.DataLoader(train_dataset, num_workers=2, batch_size=16, shuffle=True, worker_init_fn=init_fn)
train_data_loader = data.DataLoader(train_dataset, num_workers=args.workers, batch_size=args.batchsize, shuffle=True)

val_dataset=CustomDataset.ImageFolder(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True)
val_data_loader = data.DataLoader(val_dataset, num_workers=args.workers, batch_size=int(args.batchsize/3), shuffle=False)


#513

if '+' in args.layers:
    layers_sed = args.layers.split('+')[0]
    layers_dif = args.layers.split('+')[1]
else:
    layers_sed = args.layers
    layers_dif = args.layers
resnet_sed = models.__dict__['resnet' + layers_sed](pretrained=True)
resnet_dif = models.__dict__['resnet' + layers_dif](pretrained=True)
if args.model_type == 'dif':
    DifNet = model_dif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
    DifNet.model_sed = func.param_restore(DifNet.model_sed, resnet_sed.state_dict())
    DifNet.model_dif = func.param_restore(DifNet.model_dif, resnet_dif.state_dict())
elif args.model_type == 'nodif':
    DifNet = model_nodif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
    DifNet = func.param_restore(DifNet, resnet_sed.state_dict())    
else:
    print('Unknow value of model_type: {}'.format(args.model_type))
    exit()
DifNet = torch.nn.DataParallel(DifNet)
DifNet = DifNet.cuda()


optimizer = optim.SGD(DifNet.parameters(),lr=arg_lr, momentum=arg_momentum, weight_decay=arg_wdecay)
criterion = func.SegLoss(255)

result_path = parserWarpper.result_path(vars(args).copy())
date = datetime.now().strftime('%b%d_%H-%M-%S')+'_'+socket.gethostname()
directory = args.result_path + '/runs/' + result_path + date

writer = SummaryWriter(directory)
best_iou = 0
best_iou_ind = 0
best_alpha = 0
best_beta = 0

checkpoint_path = directory
#checkpoint_path = args.result_path + '/checkpoint/' + result_path + date
#directory = checkpoint_path
#if not os.path.exists(directory):
#    os.makedirs(directory)

for epoch in range(arg_epochs):
    # train for one epoch
    train(train_data_loader, DifNet, criterion, optimizer, arg_lr, epoch, arg_epochs, writer, print_freq=50)

    # evaluate on validation set
    if epoch % 10 == 0 or epoch == arg_epochs - 1:
        iou_all, iou_ind = validate_tnt(val_data_loader, DifNet, criterion, epoch, writer)

    # remember best prec@1 and save checkpoint
    is_best = iou_all > best_iou
    best_iou = iou_all if is_best else best_iou
    best_iou_ind = iou_ind if is_best else best_iou_ind
    if args.model_type == 'dif':
        best_alpha = DifNet.module.get_alpha() if is_best else best_alpha
        best_beta = DifNet.module.get_beta() if is_best else best_beta
        func.save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': DifNet.state_dict(),
            'best_iou': (best_iou, best_iou_ind),
            'best_alpha': best_alpha,
            'best_beta': best_beta,
            'optimizer': optimizer.state_dict(),
        }, is_best, tag='valid', check_dir=checkpoint_path)
    else:
        func.save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': DifNet.state_dict(),
            'best_iou': (best_iou, best_iou_ind),
            'optimizer': optimizer.state_dict(),
        }, is_best, tag='valid', check_dir=checkpoint_path)
